# reshade-misc-shaders
Shaders ported from diverse sources/requests for ReShade

# List of Shaders

- Basic CRT by the8bitpimp
- CRT Refresh by luluco250 (Originally made for the RetroTV FX Shader)
- CRTSim by J. Kyle Pittman https://github.com/MinorKeyGames/CRTSim (made for Super Win the Game, kinda broken and doesn't include the signal pass)
- Oscilloscope by MartyMcFly (Originally made for April 1st, 2015, as a fake HBAO+ Shader)
- PipBoy Shader (based on Monitor Color Shaders from DOSBox,however, using PipBoy color values)
- Quake Sky by luluco250 (Made by Request)
- Scanline by luluco250 (Made by him out of curiosity, simple Scanlines Shader)
